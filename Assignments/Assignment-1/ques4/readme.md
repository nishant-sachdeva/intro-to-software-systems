Reminders
(TA to contact regarding doubts in the question: Karandeep)
Aim :​ To create a program that allows you to create, edit, list, delete and send reminders as
desktop notifications.
Store all reminders in a delimited file(s).
Commands:
- Add reminder :
- Take command line arguments for time, body, frequency(optional)
- Your code should assign an id to the reminder- Corresponding reminder should be sent at the time entered
- List reminders :
- With no arguments, list all sorted by time of reminder
- Optional arguments for filtering by date, substring of body
- Edit reminder :
- Take arguments for id and the new fields
- Delete reminder:
- Take argument for id
Submission:​ One script to install all dependencies, scripts for each command


///////////////////////////////////////////////////////////////////////////

1. The one basic tool used is the "at" command. If that is not installed in the pc, get it done.
The dependencies script does that for Ubuntu.

2. The way to add reminders is that 
MESSAGE - TIME

3. list gives all the reminders

4. deleting a reminder requires the reminder id. You can give multiple ids if you  want.

5.  Editing  requires id and the new reminder. This reminder will be given a new id and be showed as such. 

6. Format for entering reminders  =  message-time

7. Format for editing = id-time-message

HAVE A NICE TIME :)

