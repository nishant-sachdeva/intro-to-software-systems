Creating your own search engine !!
(TA to contact regarding doubts in the question: Mugdha)
Problem statement: ​ As cool as this sounds, you will be using a set of simple bash commands
to count the the number of times a given string occurs in the source of a web page. Name your
script "searching.sh".
Example usage:
bash searching.sh searchString http://www.example.com/routexyz
Output:
searchString 42
Your script takes 2 command line arguments: the string to look up for, and the URL of the
website you want to look at.
The output contains the string to look up for, followed by its frequency of occurrence. Make sure
you handle exception and corner cases. Also, handle errors gracefully. If the right number and
type of arguments are not given, display an error message with the right usage instructions.
Submit only one file, "searching.sh".

///////////////////////////////////////////////////////

First enter the search string. Then enter the link. 
If the link is wrong, you'll  be notified.

The search results will be displayed in two ways. One for the  pattern as an  individual world and 
other as a part of any pattern.


HAVE  A NICE TIME :)

