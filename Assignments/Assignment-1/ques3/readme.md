(TA to contact regarding doubts in the question: Anubhab)
A popular time management technique uses a timer to break down work into intervals (usually
25 mins) followed by a small break.
The task is to create a BASH script to aid this process. Take a command line argument for the
number of iterations (work + break). Print notifications about the breaks (5 mins) or time to work
(25 mins) in the terminal. Every 4 iterations, include a long break (15 mins). Print notification
when all the cycles are complete.
Example usage:
bash timer.sh 2
Output structure:
#1 work
#1 break time
#2 work
#2 break time
Finished
You are free to change the content of the output to make it more meaningful as you wish but
iteration number needs to be present and general structure must be the same.
Bonus:
Instead of printing to the terminal, send notifications. You are free to use your package manager
to install additional non-default commands for this task (Only for the bonus and not the rest of
the question).


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

For the bonus part, make sure your notifications panel is on!!
Otherwise, it won't work  perfectly
Rest the  question  statement is sufficiently explanatory as to how it's gonna work