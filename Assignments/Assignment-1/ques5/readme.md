(TA to contact regarding doubts in these questions: Ali)
* Write a bash script which will do the following:
* Print ​ recursively​ the last modified date and time and the file name of the all the files in the
current working directory in a given format.(Use piping)
Example Output: If your current directory contains two files A and B. Then
output should be as follows:
Last_Modified_Date_of_A Last_Modified_Time_of_A Relative_path_of_A
Last_Modified_Date_of_B Last_Modified_Time_of_B Relative_path_of_B
* Find all the commands that starts the word “lo” and store the commands and their small
descriptions in a file named your_roll_no.txt(For eg: 201801159.txt). Each line in the file should
contain the command along with its small description.
* Display the number of lines and the length of the longest line in the above created File.
* Replace all the occurrences of the word “function” with “method” in the above file. In
addition, create a backup of your original file “your_roll_no.txt”.
Submission:​ script1.sh, your_roll_no.txt, backup file
* Adult income census was conducted and the output of the census contains 2 csv files named
file1.csv and file2.csv. Write a script named script2.sh which will perform the following
operations:
* Concatenate these 2 files into a single file named target_file.csv
* Create a header file named header.csv which will contain:
"Index,Age,workclass,fnlwgt,education,education-num,marital-status,occupation,relationship,rac
e,sex,capital-gain,capital-loss,native-country,class".
Add this header file at the beginning of the target_file.csv* The target_file.csv would contain some missing values denoted by ‘?’, replace these
missing values with your roll number.
Submission:​ script2.sh, target_file.csv


The question statement seem pretty self explanatory about what to expect.

In the list file, some commands may not have any description. This is because we did not find any man page files for that command

